module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {
        if (json.src) {
            ctx.attr('style', 'background-image: url(\'' + json.src + '\')');
        }
    });
};