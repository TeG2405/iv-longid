({
    mustDeps: [
        {

        }
    ],
    shouldDeps: [
        {
            block: "people",
            elem: ['count', 'title', 'item', 'description', 'icon', 'box'],
            mods: ['image']
        }
    ]
})