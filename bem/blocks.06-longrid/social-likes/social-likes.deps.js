({
    mustDeps: [
        {block: 'lazysizes'},
        {block: 'typography'}
    ],
    shouldDeps: [
        {
            'block': 'fa'
        },
        {
            'block': 'social-likes',
            'elem': ['button', 'counter', 'icon', 'widget']
        }
    ]
})