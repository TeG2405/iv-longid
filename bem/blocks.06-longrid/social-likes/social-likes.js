/*=include ../../../bower_components/social-likes/src/social-likes.js */

+function($){
    var cls = 'social-likes';
    $(document).on('lazybeforeunveil', function(e){
        $(e.target)
            .closest('.social-likes')
            .socialLikes();
    });
}(jQuery);

