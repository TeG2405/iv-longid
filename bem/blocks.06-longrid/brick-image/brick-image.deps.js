({
    mustDeps: [
        {block: 'typography'},
        {block: 'lazyload'},
        {block: 'lazysizes', mod: 'unveilhooks'},
        {block: 'lazysizes', mod: 'bgset'}
    ],
    shouldDeps: [
        {}
    ]
})