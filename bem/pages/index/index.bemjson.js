module.exports = {
    block: 'page',
    title: 'Базовый',
    id: 'base',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../../../bem/pages/_common/header.bemjson.js'),
        {block: 'main', content: [
            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [

                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры используйте false если заголовок единственный элемент блока
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        content: [
                            {block: 'h', size: '2', content: [
                                'День первый. Регистрируем Битрикс24, продаем идею ответственным. (Стандарт)',
                            ]}
                        ]
                    })
                ]
            }),
            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    {elem: 'flex', mods: {'justify-content': 'space-between'}, content: [
                        require('../../../bem/pages/_common/longrids/tile/tile.bemjson.js')
                    ]}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base pbn', // допустимые значения [base, invert];
                content: [

                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры используйте false если заголовок единственный элемент блока
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        content: [
                            {block: 'h', size: '2', content: [
                                'День первый. Регистрируем Битрикс24, продаем идею ответственным. (Управляемый)',
                            ]}
                        ]
                    })
                ]
            }),
            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    {elem: 'flex', mods: {'justify-content': 'space-between'}, content: [
                        require('../../../bem/pages/_common/longrids/tile/tile.bemjson.js')
                    ]}
                ]
            }),



            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    {block: 'h', size: '2', content: [
                        'День первый. Регистрируем Битрикс24, продаем идею ответственным. (Задуманно было так)',
                    ]},
                    {elem: 'flex', mods: {'justify-content': 'space-between'}, content: [
                        require('../../../bem/pages/_common/longrids/tile/tile.bemjson.js')
                    ]}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    {block: 'h', size: '2', content: [
                        'День первый. Регистрируем Битрикс24, продаем идею ответственным. (Задуманно было так)',
                    ]},
                    {block: 'p', content: 'Если в организации нет устоявшихся процессов или информационной системы (вроде Битрикс24), в ней царит неорганизованность и хаос. Дела делаются, но как — неясно, без системы и общих правил. Когда процессы становятся достаточно сложны, здравого смысла и стихийно сложившихся правил недостаточно, чтобы все работало хорошо.'},
                    {block: 'p', content: 'Если в организации нет устоявшихся процессов или информационной системы (вроде Битрикс24), в ней царит неорганизованность и хаос. Дела делаются, но как — неясно, без системы и общих правил. Когда процессы становятся достаточно сложны, здравого смысла и стихийно сложившихся правил недостаточно, чтобы все работало хорошо.'},
                    {elem: 'flex', mods: {'justify-content': 'space-between'}, content: [
                        require('../../../bem/pages/_common/longrids/tile/tile.bemjson.js')
                    ]}
                ]
            }),
        ]},
        require('../../../bem/pages/_common/footer.bemjson.js'),
        require('../../../bem/pages/_common/modals/mail.bemjson.js')
    ]
};
