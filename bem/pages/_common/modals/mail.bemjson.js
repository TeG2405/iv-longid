module.exports = [
    {mix:[
        {block: 'modal'},
        {block: 'fade'}
    ], attrs: {
        tabindex:"-1",
        role:"dialog",
        id:"feedback-1"
    }, content: [
        {mix: [
            {block: 'modal-dialog'},
            {block: 'modal-lg'}
        ], content: [
            {mix: [{block: 'modal-content'}], content: [
                {mix: [{block: 'modal-header'}], content: [
                    '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
                    {block: 'h', size: '4', attrs: {style: 'max-width: 550px; margin: 0 auto;'}, content: 'Подпишитесь на ежемесячную новостную рассылку'}
                ]},
                {mix: [{block: 'modal-body'}], content: [
                    {block: 'form-horizontal', attrs: {style: 'max-width: 550px; margin: 0 auto;'}, content: [

                        {block: 'form-group', arParam: {
                            label: 'Ваше имя',
                            grid: [12, 8, 8, 8]
                        }, content: [
                            {block: 'form-control', placeholder: 'Иван Иванов'}
                        ]},

                        {block: 'form-group', arParam: {
                            label: 'Email',
                            grid: [12, 8, 8, 8]
                        }, content: [
                            {block: 'form-control', placeholder: ''}
                        ]},

                        {block: 'form-group', arParam: {
                            label: 'Выберите рассылку',
                            grid: [12, 8, 8, 8]
                        }, content: [
                            {block: 'checkbox', content: [
                                'Полезные статьи и новые проекты',
                                {tag: 'span', mix: {block: 'text-muted'}, content: 'В нашем блоге много статей про 1С-Битрикс, Битрикс24 и интернет-маркетинг и этот список постоянно пополняется. Подпишитесь и раз в месяц мы будем присылать вам самые интересные статьи, а также наши новые проекты'}
                            ]},
                            {block: 'checkbox', content: [
                                'Семинары',
                                {tag: 'span', mix: {block: 'text-muted'}, content: 'В нашем блоге много статей про 1С-Битрикс, Битрикс24 и интернет-маркетинг и этот список постоянно пополняется. Подпишитесь и раз в месяц мы будем присылать вам самые интересные статьи, а также наши новые проекты'}
                            ]},
                            {block: 'checkbox', content: [
                                'Акции',
                                {tag: 'span', mix: {block: 'text-muted'}, content: 'В нашем блоге много статей про 1С-Битрикс, Битрикс24 и интернет-маркетинг и этот список постоянно пополняется. Подпишитесь и раз в месяц мы будем присылать вам самые интересные статьи, а также наши новые проекты'}
                            ]}
                        ]},

                        {block: 'form-group', arParam: {
                            label: 'Защитный код',
                            grid: [12, 8, 8, 8]
                        }, content: [
                            {block: 'input-group', content: [
                                {block: 'form-control', placeholder: 'Код с картники'},
                                {block: 'input-group-addon', content: [
                                    {block: 'img', attrs:{
                                        style: 'height: 2em; margin: -0.5em'
                                    }, src: '../../../upload/captcha.jpg'}
                                ]}
                            ]}
                        ]},

                        {block: 'form-group', arParam: {
                            label: '',
                            grid: [12, 8, 8, 8]
                        }, content: [
                            {block: 'btn', mods: {color: 'danger', size: 'lg'}, content: 'Отправить'}
                        ]}
                    ]}
                ]}
            ]}
        ]}
    ]}
];