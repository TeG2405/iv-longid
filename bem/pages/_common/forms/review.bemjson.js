module.exports = [
    {mix: [
        {block: 'row'}
    ], content: [
        {mix: [
            {block: 'col-xs-12'},
            {block: 'col-sm-8'}
        ], content: [
            {block: 'form-horizontal', content: [

                {block: 'form-group', arParam: {
                    label: 'Ваше имя',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'form-control', placeholder: 'Иван Иванов'}
                ]},

                {block: 'form-group', arParam: {
                    label: 'Компания',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'form-control', placeholder: 'Компания'}
                ]},

                {block: 'form-group', arParam: {
                    label: 'Телефон',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'form-control', placeholder: ''}
                ]},

                {block: 'form-group', arParam: {
                    label: 'Email',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'form-control', placeholder: ''}
                ]},

                {block: 'form-group', arParam: {
                    label: 'Техническое задание или видение проекта, при наличии',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'form-control-static', content: [
                        {content: [
                            {block: 'btn', mods: {color: 'default', size: 'sm'}, attrs: {type: false}, tag: 'label', content: [
                                {block: 'sr-only', tag: 'input', attrs: {
                                    type: 'file',
                                    multiple: true,
                                    onchange: 'this.nextSibling.innerHTML = (this.value || "Файл не выбран")'
                                }},
                                {block: 'span', content: 'Файл не выбран'}
                            ]}
                        ]},
                        {block: 'small', content: 'Доступно: doc, rtf, pdf, txt, docx.'}
                    ]}
                ]},

                {block: 'form-group', arParam: {
                    label: 'Сколько у вас сотрудников',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'checkbox', content: 'Да'},
                    {block: 'checkbox', content: 'Нет'}
                ]},
                {block: 'form-group', arParam: {
                    label: 'Требуется консультация перед внедрением? ',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'radio', content: 'Да'},
                    {block: 'radio', content: 'Нет'}
                ]},
                {block: 'form-group', arParam: {
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'form-control', tag: 'textarea', placeholder: 'Ваше сообщение'}
                ]},
                {block: 'form-group', arParam: {
                    label: 'Защитный код',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'input-group', content: [
                        {block: 'form-control', placeholder: 'Код с картники'},
                        {block: 'input-group-addon', content: [
                            {block: 'img', attrs:{
                                style: 'height: 2em; margin: -0.5em'
                            }, src: '../../../upload/captcha.jpg'}
                        ]}
                    ]}
                ]},
                {block: 'form-group', arParam: {
                    label: '',
                    grid: [12, 7, 8, 7]
                }, content: [
                    {block: 'btn', mods: {color: 'danger', size: 'lg'}, content: 'Отправить'}
                ]}
            ]},
            {block: 'visible-xs mvl pvl'},
        ]},
        {mix: [
            {block: 'col-xs-12'},
            {block: 'col-sm-4'}
        ], content: [
            {block: 'h', size: '4', content: 'Образцы документов'},
            {content: [
                'Договор на разработку <br/>«Готовое решение»',
                {block: 'br'},
                {block: 'a', content: 'Посмотреть'}
            ]},
            {block: 'br'},
            {content: [
                'Договор на разработку <br/>«Готовое решение»',
                {block: 'br'},
                {block: 'a', content: 'Посмотреть'}
            ]},
            {block: 'br'},
            {content: [
                'Договор на разработку <br/>«Готовое решение»',
                {block: 'br'},
                {block: 'a', content: 'Посмотреть'}
            ]}
        ]}
    ]}
];