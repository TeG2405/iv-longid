var fs = require('fs');
module.exports = [
    {block: 'header', content: [
        {elem: 'container', mix: {block: 'container', mods:{fluid: 'false'}}, content: [
            { elem: "row", content: [
                { elem: "logo", content: [
                    { block: "navigation-toggle", navigationID: 'navigation-1' },

                    { block: "logo", content: "Профильно. Прибыльно. С удовольствием.",
                        src: {
                            xs: '../../../upload/logo__name_xs.png',
                            sm: '../../../upload/logo__name_sm.png',
                            md: '../../../upload/logo__name.png'
                        }
                    }
                ]},
                { elem: "bottom", content: [
                    { elem: "row", content: [
                        { elem: "feedback", content: [
                            { elem: "feedback", content: [
                                { block: "feedback", modalID: "feedback-1", content: "Написать нам" }]
                            }]
                        },
                        { elem: "search", content: [
                            { block: "search" }
                        ]}
                    ]}
                ]},
                { elem: "phone", content: [
                    { block: "phone-list",
                        numbers: [
                            { prefix: "+7 (495)", number: "648-57-90" },
                            { prefix: "+7 (495)", number: "95-99-99" }
                        ]
                    }
                ]}
            ]}
        ]},
        {block: "navigation",  navigationID: 'navigation-1',  content: [
            {elem: "container", content: [
                {elem: 'collapse', content: [
                    {elem: "nav", content: [
                        { elem: "item", content: "Портфолио" },
                        { elem: "item", mods: {'state': 'active'}, content: "Отзывы" },
                        { elem: "item", content: "1С-Битрикс"  },
                        { elem: "item", content: "Блог" },
                        { elem: "item", content: "Цены" },
                        { elem: "item", content: "Мы" },
                        { elem: "item", content: "Тиражные решения"  },
                        { elem: "item", content: "Вакансии"  },
                        { elem: "item", content: "Контакты" }
                    ]},
                    {elem: 'shadow'}
                ]},
                {elem: 'helper' }
            ]}
        ]},
        {block: 'directions-sections', content: [
            {elem: 'container', content: [
                {elem: 'inner', content: [
                    {elem: 'item', content: [
                        {elem: 'img', content: [
                            {elem: 'icon', content: [
                                {block: 'svg', content: fs.readFileSync('./upload/php.svg', 'utf8', function (err, data) {if (err) throw err; return data})}
                            ]}
                        ]},
                        {elem: 'title', content: 'Разработка'}
                    ]},
                    {elem: 'item', content: [
                        {elem: 'img', content: [
                            {elem: 'icon', content: [
                                {block: 'svg', content: fs.readFileSync('./upload/marketing.svg', 'utf8', function (err, data) {if (err) throw err; return data})}
                            ]}
                        ]},
                        {elem: 'title', content: 'Маркетинг'}
                    ]},
                    {elem: 'item', content: [
                        {elem: 'img', content: [
                            {elem: 'icon', content: [
                                {block: 'svg', content: fs.readFileSync('./upload/bitrix.svg', 'utf8', function (err, data) {if (err) throw err; return data})}
                            ]}
                        ]},
                        {elem: 'title', content: 'Битрикс 24'}
                    ]},
                    {elem: 'item', content: [
                        {elem: 'img', content: [
                            {elem: 'icon', content: [
                                {block: 'svg', content: fs.readFileSync('./upload/brands.svg', 'utf8', function (err, data) {if (err) throw err; return data})}
                            ]}
                        ]},
                        {elem: 'title', content: 'Брендинг'}
                    ]},
                    {elem: 'item', content: [
                        {elem: 'img', content: [
                            {elem: 'icon', content: [
                                {block: 'svg', content: fs.readFileSync('./upload/support.svg', 'utf8', function (err, data) {if (err) throw err; return data})}
                            ]}
                        ]},
                        {elem: 'title', content: 'Поддержка'}
                    ]}
                ]}
            ]}
        ]},
        {block: 'directions-items', content: [
            {elem: 'container', content: [
                {elem: 'inner', content: [
                    {elem: 'item', content: 'Сайты компаний'},
                    {elem: 'item', content: 'Интернет-магазины'},
                    {elem: 'item', content: 'Сложные проекты'},
                    {elem: 'item', content: 'Enterprise проекты'}
                ]}
            ]}
        ]}
    ]},

];