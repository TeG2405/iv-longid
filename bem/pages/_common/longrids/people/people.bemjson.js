module.exports = [
    {block: 'people', content: [
        {elem: 'item', content: [
            {elem: 'title', content: 'Люди'},
            {elem: 'box', content:[
                {elem: 'icon', content: [
                    {block: 'img', src: '../../../upload/people__item-1.png', mods: {
                        lazyload: true,
                        responsive: true
                    }},
                    {elem: 'count', content: '2'}
                ]}
            ]},
            {elem: 'description', content: 'Высококлассных специалистаа'}
        ]},
        {elem: 'item', content: [
            {elem: 'title', content: 'Технологии'},
            {elem: 'box', content:[
                {elem: 'icon', content: [
                    {block: 'img', src: '../../../upload/people__item-2.png', mods: {
                        lazyload: true,
                        responsive: true
                    }},
                    {elem: 'count', content: '99'}
                ]}
            ]},
            {elem: 'description', content: 'Уникальных проекта'}
        ]},
        {elem: 'item', content: [
            {elem: 'title', content: 'Менеджмент'},
            {elem: 'box', content:[
                {elem: 'icon', content: [
                    {block: 'img', src: '../../../upload/people__item-3.png', mods: {
                        lazyload: true,
                        responsive: true
                    }},
                    {elem: 'count', content: '2'}
                ]}
            ]},
            {elem: 'description', content: 'Управление по целям SMART'}
        ]}
    ]}
];