module.exports = function(json){
    return [
        json &&
        {block: 'brick-image', height: 'auto', content: [
            {elem: 'background', mods: {lazyload: true}, src: json.src, content: [
                (function(offset){
                    var temp = [];
                    for(var i=0; i<offset; i++) temp.push({block: 'br'});
                    return temp;
                })(json.offset),
                {elem: 'container', content: [
                    {elem: 'inner', content: [
                        json.header && {elem: 'header', tag: 'h1', content: json.header},
                        json.description && {elem: 'description', content: json.description},
                    ]},
                    (function(offset){
                        var temp = [];
                        for(var i=0; i<offset; i++) temp.push({block: 'br'});
                        return temp;
                    })(json.offset),
                    json.icon && {elem: 'icon'}
                ]},
            ]}
        ]}
    ];
};