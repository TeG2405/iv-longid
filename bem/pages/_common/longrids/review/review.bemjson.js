var items = [];
for(var i=0; i< 4; i++){
    items.push([
        {elem: 'item', content: [
            { elem: 'flex', tag: 'a', attrs:{href: '#'}, content: [
                {elem: 'img', content: [
                    {block: 'img', mods: {
                        lazyload: true,
                        responsive: true
                    }, src: '../../../upload/review-list-img-1.jpg'}
                ]},
                {elem: 'description', content: [
                    {elem: 'table', content: [
                        {elem: 'cell', content: 'Юрий Устинов, руководитель направления нефинансовых сервисов «Альфа-банка»'}
                    ]}
                ]}
            ]}
        ]}
    ]);
}
module.exports = [
    {block: 'review-list', content: [
        items,
        {elem: 'item'}
    ]}
];