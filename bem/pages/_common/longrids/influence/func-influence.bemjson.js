module.exports = function(json){
    return [
        json &&
        {block: 'influence', content: [
            {elem: 'wrapper', content: [
                json.title && {elem: 'title', content: json.title},
                json.description && {elem: 'description', content: json.description}
            ]},
            json.icon && {elem: 'icon', content: json.icon},
            json.content && {elem: 'action', content: json.content}
        ]}
    ];
};