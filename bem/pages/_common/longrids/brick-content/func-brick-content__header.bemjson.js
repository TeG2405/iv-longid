module.exports = function(json){
    return [
        json.content &&
        {elem: 'icon', content: [
            json.content,
            json.sub && {block: 'span', content: json.sub}
        ]}
    ]
};