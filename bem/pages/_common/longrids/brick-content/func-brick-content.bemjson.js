module.exports = function(json){
    return [
        {block: 'brick-content', mods: {
            bg: json.bgColor || false,
            color: json.textColor || 'base',
            lazyload: !!json.src

        }, src: json.src, content: [
            {elem: 'wrapper', mods: {
                bg: json.wrapper || false
            }, content: [
                {elem: 'container', content: [
                    json.content
                ]}
            ]}
        ]}
    ]
};