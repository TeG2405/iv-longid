module.exports = function(json){
    return [
        json &&
        {block: 'quote', mods: {variant: json.variant || false}, content: [
            json.content  && {elem: 'text', content: json.content},
            json.author   && {elem: 'author', content: json.author},
            json.position && {elem: 'position', content: json.position}
        ]}
    ]
};