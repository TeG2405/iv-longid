module.exports = [
    {block: 'brick-content', mods: {
        bg: 'blue-base',
        color: 'invert'
    }, content: [
        {elem: 'container', content: [
            {block: 'quote', mods: {variant: 'divide'}, content: [
                {elem: 'text', content: 'Внедрение Битрикс24 основано на конструктивном настрое и трудолюбии сотрудников. Работа с людьми – это про любовь, а не про насилие.'},
                {elem: 'author', content: 'Степан Овчинников'},
                {elem: 'position', content: 'Генеральный директор'}
            ]}
        ]}
    ]}
];