var items = [];
for(var i=0; i< 5; i++){
    items.push([
        {mix: [
            {block: 'col-xs-12'},
            {block: 'col-sm-5'}
        ], content: [
            {block: 'br'},
            {elem: 'icon', content: [
                {block: 'h', size: '4', content: [
                    'Расскажите о целях и планах',
                    {block: 'icon', mix: {
                        block: 'fa-2x'
                    }, icon: 'docs'}
                ]},
                {block: 'span'}
            ]},
            {block: 'p', content: 'Когда предложение принимается, мы заключаем договор и начинаем делать сайт. Договор — основа цивилизованных отношений.'}
        ]}
    ]);
}
module.exports = [
    items
];