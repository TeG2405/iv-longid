var items = [];
for(var i=0; i< 10; i++){
    items.push([
        {elem: 'item', content: [
            {tag: 'a', attrs: {href: '#'}, content: [
                {elem: 'img', content: [
                    {block: 'img', mods: {
                        lazyload: true,
                        responsive: true
                    }, src: '../../../upload/work-list-img-1.jpg'}
                ]},
                {elem: 'title', content: 'Магазин Shell'}
            ]}
        ]}
    ]);
}
module.exports = [
    {block: 'work-list', content: [
        items,
        {elem: 'item'},
        {elem: 'item'},
        {elem: 'item'}
    ]}
];