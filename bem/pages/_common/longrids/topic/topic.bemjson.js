var items = [];
for(var i=0; i< 6; i++){
    items.push([
        {elem: 'item', content: [
            {elem: 'author', content: 'Степан Овчинников'},
            {elem: 'link', content: [
                {block: 'a', content: 'Как работает обмен товарами 1С УТ и Битрикс24'}
            ]}
        ]}
    ]);
}
module.exports = [
    {block: 'topic', content: [
        items,
        {elem: 'item'},
        {elem: 'item'}
    ]}
];