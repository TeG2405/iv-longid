module.exports = [
    {block: 'footer', content: [
        { elem: "container", content: [
            { elem: "row", content: [
                { elem: 'contacts', content: [
                    { elem: "phone", content: "(8442) 95-99-99", mask: "7-8442-95-99-99" },
                    { elem: "mail", content: "info@intervolga.ru" },
                    { elem: "address", content: "Волгоград, наб. 62-й Армии, дом 6 (ИКРА, Пиранья)" }
                ]},
                { elem: "right", content: [
                    { elem: "copyright", content: "© ИНТЕРВОЛГА (Волгоград), 2003&nbsp;—&nbsp;2016" },
                    { block: "list-inline", mix: {block: 'mvn'}, content: [
                        {block: 'a', content: 'Контакты'},
                        {block: 'a', content: 'Ростов'},
                        {block: 'a', content: 'Москва'},
                        {block: 'a', content: 'Все'}
                    ]}
                ]},
                { elem: "social", content: [
                    { block: "social", content: [
                        { elem: "item", fa: "vk", content: "vk.com" },
                        { elem: "item", fa: "facebook", content: "facebook.com" },
                        { elem: "item", fa: "twitter", content: "twitter.com" },
                        { elem: "item", fa: "envelope", content: "mailto:intervolga@mail.ru"},
                        { elem: "item", fa: "youtube", content: "youtube.com" },
                        { elem: "item", fa: "instagram", content: "instagram.com" },
                        { elem: "item", mods: { "bitrix": true }, content: "Быстро с битрикс" }
                    ]}
                ]}
            ]}
        ]}
    ]}
];