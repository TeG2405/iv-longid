module.exports = {
    block: 'page',
    title: 'Базовый',
    id: 'base',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../../../bem/pages/_common/header.bemjson.js'),
        {block: 'main', content: [
  
            // Изображение банер;
            // offset не обязательный параметр добавляющий высоты блоку, шаг равен (высоте базовой линии * 2)
            require('../../../bem/pages/_common/longrids/brick-image/func-brick-image.bemjson.js')({
                header: 'Разработка сайтов',
                description: [{block: 'i', content: [
                    'Мы занимаемся запуском веб-проектов. Обычно это довольно крупные корпоративные сайты или интернет-магазины. Иногда — социальные сети, аукционы или биржи товаров и услуг.'
                ]}],
                src: {
                    xs: '../../../upload/brick-image-xs.jpg',
                    sm: '../../../upload/brick-image-sm.jpg',
                    md: '../../../upload/brick-image-md.jpg',
                    lg: '../../../upload/brick-image-lg.jpg'
                },
                offset: 2,
                icon: true
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    //Блок Цитаты
                    require('../../../bem/pages/_common/longrids/quote/func-quote.bemjson.js')({
                        content: 'Внедрение Битрикс24 основано на конструктивном настрое и трудолюбии сотрудников. Работа с людьми – это про любовь, а не про насилие.',
                        author: 'Степан Овчинников',
                        position: 'Генеральный директор'
                    })
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'p', content: 'В <abbr>статье</abbr> рассказано, как это сделать с помощью Битрикс24. Советуем прочитать Манифест внедрения Битрикс24 , где кратко изложены основные принципы работы – “заповеди”.  Для опытных администраторов и IT-сотрудников крупных компаний рекомендуем изучить очень качественный (хотя объемный и сложный) учебный курс по внедрению портала от 1С-Битрикс. '},
                    {block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [

                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        content: [
                            {block: 'h', size: '2', content: [
                                'День первый. Регистрируем Битрикс24, продаем идею ответственным.'
                            ]}
                        ]
                    }),
                    {block: 'mbm'},
                    //Блок Цитаты ()
                    require('../../../bem/pages/_common/longrids/quote/func-quote.bemjson.js')({
                        content: [
                            'И сказал Бог: да будет свет. И стал свет.',
                            {block: 'br'},
                            'И увидел Бог свет, что он хорош: и отделил Бог свет от тьмы.'
                        ],
                        variant: 'divide'
                    })
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'p', content: 'В статье рассказано, как это сделать с помощью Битрикс24. Советуем прочитать Манифест внедрения Битрикс24 , где кратко изложены основные принципы работы – “заповеди”.  Для опытных администраторов и IT-сотрудников крупных компаний рекомендуем изучить очень качественный (хотя объемный и сложный) учебный курс по внедрению портала от 1С-Битрикс. '},
                    {block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}
                ]
            }),

            // Изображение банер;
            // offset не обязательный параметр добавляющий высоты блоку, шаг равен (высоте базовой линии * 2)
            require('../../../bem/pages/_common/longrids/brick-image/func-brick-image.bemjson.js')({
                header: 'Для кого будет полезно мероприятие?',
                description: [
                    'Для директоров по маркетингу и руководителей промышленных и производственных предприятий.',
                    {block: 'br'},
                    'Для современного предприятия работать в одном, пусть и родном, регионе — путь к регрессу. Рано или поздно ниша будет переполнена. Понадобится расширять географию.'
                ],
                src: {
                    xs: '../../../upload/brick-image-xs.jpg',
                    sm: '../../../upload/brick-image-sm.jpg',
                    md: '../../../upload/brick-image-md.jpg',
                    lg: '../../../upload/brick-image-lg.jpg'
                },
                offset: 0,
                icon: false
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'picture', mods: {lazyload: true, as: 'img'}, src: {
                        xs: '../../../upload/content-image-2.jpg',
                        sm: '../../../upload/content-image-2.jpg',
                        md: '../../../upload/content-image-2.jpg',
                        lg: '../../../upload/content-image-2.jpg'
                    }},
                    {block: 'p', content: 'Если в организации нет устоявшихся процессов или информационной системы (вроде Битрикс24), в ней царит неорганизованность и хаос. Дела делаются, но как — неясно, без системы и общих правил. Когда процессы становятся достаточно сложны, здравого смысла и стихийно сложившихся правил недостаточно, чтобы все работало хорошо.'},
                    {block: 'p', content: 'В этот момент руководителю мучительно не хватает глубины понимания, фактов, обратной связи. Он точно знает что люди могут работать лучше, но не знает как именно этого добиться.'},
                    {block: 'p', content: 'Нужна система работы, устоявшиеся традиции эффективного расходования времени. Такая система рождается постепенно, с обязательным участием наиболее толковых и инициативных сотрудников. Чтобы начать, требуется прозрачность работы и снижение потерь на коммуникации. Нужен свет, который озарит происходящее и сделает светлыми головы. Если в организации нет устоявшихся процессов или информационной системы (вроде Битрикс24), в ней царит неорганизованность и хаос. '}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [

                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры используйте false если заголовок единственный элемент блока
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        content: [
                            {block: 'h', size: '2', content: [
                                'День первый. Регистрируем Битрикс24, продаем идею ответственным.',
                            ]}
                        ]
                    })
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'p', content: 'В статье рассказано, как это сделать с помощью Битрикс24. Советуем прочитать Манифест внедрения Битрикс24 , где кратко изложены основные принципы работы – “заповеди”.  Для опытных администраторов и IT-сотрудников крупных компаний рекомендуем изучить очень качественный (хотя объемный и сложный) учебный курс по внедрению портала от 1С-Битрикс. '},
                    {block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                // поле враппер используется для дополнительного выделения контентной части
                wrapper: 'gray-lighter', // имя переменной цаета или оставте пустым,
                content: [
                    // Блок призывающий к действию
                    require('../../../bem/pages/_common/longrids/influence/func-influence.bemjson.js')({
                        title: 'Есть что сказать?',
                        description: 'Нам всегда важно ваше мнение! ',
                        icon: {block: 'icon', icon: 'review', mix: {block: 'fa-5x'}},
                        content: [
                            {block: 'btn', mods: {block: true, size: 'lg', color: 'danger'}, content: 'Оставить свой отзыв'}
                        ]
                    })
                ]
            }),
            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                // поле враппер используется для дополнительного выделения контентной части
                wrapper: 'gray-lighter', // имя переменной цаета или оставте пустым,
                content: [
                    // Блок призывающий к действию
                    require('../../../bem/pages/_common/longrids/influence/func-influence.bemjson.js')({
                        title: 'Хотите получать новые статьи по почте?',
                        icon: {block: 'icon', icon: 'mail', mix: {block: 'fa-5x'}},
                        content: [
                            {block: 'btn', mods: {block: true, size: 'lg', color: 'danger'}, content: 'Подпишитесь на рассылку'}
                        ]
                    })
                ]  
            }), 
            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                // поле враппер используется для дополнительного выделения контентной части
                wrapper: 'gray-lighter', // имя переменной цаета или оставте пустым,
                content: [
                    // Блок призывающий к действию
                    require('../../../bem/pages/_common/longrids/influence/func-influence.bemjson.js')({
                        title: 'Нет времени ждать?',
                        description: '<small>Вы уже готовы создать сайт, которым будете гордиться?<br>Тогда не будем тратить время даром.</small>',
                        content: [
                            {block: 'btn', mods: {block: true, size: 'lg', color: 'danger'}, content: 'Заказать сайт'}
                        ]
                    })
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'h', size: '2', content: 'Заголовок в тексте!!!!!!!!!!!!!!!!'},
                    {block: 'p', content: 'В статье рассказано, как это сделать с помощью Битрикс24. Советуем прочитать Манифест внедрения Битрикс24 , где кратко изложены основные принципы работы – “заповеди”.  Для опытных администраторов и IT-сотрудников крупных компаний рекомендуем изучить очень качественный (хотя объемный и сложный) учебный курс по внедрению портала от 1С-Битрикс. '},
                    {block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: 'gray-base', // имя переменной цаета или оставте пустым
                textColor: 'invert', // допустимые значения [base, invert];
                content: [

                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры используйте false если заголовок единственный элемент блока
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        sub: ' ',
                        content: [
                            {block: 'h', size: '2', content: [
                                'День третий. Лайки и задачи.',
                                {block: 'icon', icon: 'video'}
                            ]}
                        ]
                    }),
                    {block: 'p', content: 'Нужна система работы, устоявшиеся традиции эффективного расходования времени. Такая система рождается постепенно, с обязательным участием наиболее толковых и инициативных сотрудников.'},
                    {block: 'p', content: 'Чтобы начать, требуется прозрачность работы и снижение потерь на коммуникации. Нужен свет, который озарит происходящее и сделает светлыми головы.'},
                    {block: 'embed-responsive', mods: {'ratio': '16by9'}, content: [
                        {elem: 'item', tag: 'iframe', attrs: {src: '//www.youtube.com/embed/yvRn76Fqyzc?rel=0&showinfo=0'}}
                    ]}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                // поле враппер используется для дополнительного выделения контентной части
                wrapper: 'gray-lighter', // имя переменной цаета или оставте пустым,
                content: [
                    // Блок призывающий к действию
                    require('../../../bem/pages/_common/longrids/influence/func-influence.bemjson.js')({
                        title: 'Понравился пост?',
                        description: 'Поделитесь ссылкой с друзьями!',
                        icon: {block: 'icon', icon: 'mouthpiece', mix: {block: 'fa-5x'}},
                        content: [
                            { block: 'social-likes', content: [
                                {item: 'likes', mix: {block: 'vkontakte'}},
                                {item: 'likes', mix: {block: 'facebook'}},
                                {item: 'likes', mix: {block: 'plusone'}},
                                {item: 'likes', mix: {block: 'twitter'}}
                            ]}
                        ]
                    })
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'p', content: 'В статье рассказано, как это сделать с помощью Битрикс24. Советуем прочитать Манифест внедрения Битрикс24 , где кратко изложены основные принципы работы – “заповеди”.  Для опытных администраторов и IT-сотрудников крупных компаний рекомендуем изучить очень качественный (хотя объемный и сложный) учебный курс по внедрению портала от 1С-Битрикс. '},
                    {block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'h', size: '2', content: 'Чем мы можем вам помочь'},
                    {block: 'img', mix: {block: 'pull-right'}, attrs: {width: '342'}, src: '../../../upload/content-image-3.png'},
                    [
                        {block: 'span', content: 'Предоставить лицензионный ключ на облачную или коробочную версию, добавить сотрудников и настроить права доступа, сконфигурировать CRM под вашу компанию, импортировать контакты, установить систему на сервер (для  коробочной версии), интегрировать с Active Directory или 1С: Зарплата и управление персоналом.'},
                        {block: 'mvm', content: [
                            {block: 'i', content: 'ИНТЕРВОЛГА – золотой сертифицированный партнер 1С-Битрикс, умеет все это делать и имеет юридические права на сопровождение.'}
                        ]},
                        {block: 'b', content: 'Стоимость: '},
                        {block: 'span', content: 'от 8 тысяч рублей. '},
                        {block: 'a', content: 'Прайс-лист.'},
                        {block: 'br'},
                        {block: 'mtm', content: [
                            {block: 'btn', mods: {color: 'danger'}, content: 'Заказать консультацию по покупке и установке Битрикс24'}
                        ]}
                    ],
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: 'gray-lighter', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Доп. элемент flex
                    {elem: 'flex', mods: {'align-items': 'flex-end'}, content: [

                        {mix: [
                            {block: 'col-xs-12'},
                            {block: 'col-md-6'}
                        ], content: [
                            {block: 'img', mods: {
                                lazyload: true,
                                responsive: true
                            }, src: '../../../upload/content-image-4.png'}
                        ]},

                        {mix: [
                            {block: 'col-xs-12'},
                            {block: 'col-md-6'}
                        ], content: [
                            {block: 'h', size: '4', content: 'Провести корпоративное обучение по Битрикс24. Как правило, достаточно 5 часов работы нашего специалиста с руководящим составом '},
                            [
                                {block: 'span', content: 'Предоставить лицензионный ключ на облачную или коробочную версию, добавить сотрудников и настроить права доступа, сконфигурировать CRM под вашу компанию, импортировать контакты, установить систему на сервер (для  коробочной версии), интегрировать с Active Directory или 1С: Зарплата и управление персоналом.'},
                                {block: 'mvm', content: [
                                    {block: 'i', content: 'ИНТЕРВОЛГА – золотой сертифицированный партнер 1С-Битрикс, умеет все это делать и имеет юридические права на сопровождение.'}
                                ]},
                                {block: 'b', content: 'Стоимость: '},
                                {block: 'span', content: 'от 8 тысяч рублей. '},
                                {block: 'a', content: 'Прайс-лист.'}
                            ]
                        ]}
                    ]}

                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'h', size: '2', content: 'Рекомендуем к изучению наши самые свежие статьи'},
                    {block: 'img', mix: {block: 'pull-right'}, attrs: {width: '342'}, src: '../../../upload/content-image-5.png'},
                    {content: [
                        {block: 'ul', content: [
                            'Но не стоит дольше нескольких дней позволять Битрикс24 быть просто “общим чатом” для компании.',
                            'Показывайте пример хорошего стиля: понятное название, указан срок, подробности, ответственный и помощники.'
                        ]}
                    ]},
                    {block: 'br'},
                    {content: [
                        {block: 'i', content: 'О внешних интеграциях, большом внедрении и доработках  Битрикс24:'},
                    ]},
                    {block: 'br'},
                    {content: [
                        {block: 'ol', content: [
                            {block: 'a', content: 'Большое внедрение Битрикс24. Проблемы и решения'},
                            {block: 'a', content: 'Большое внедрение и настройка CRM Битрикс. Внешняя интеграция'},
                            {block: 'a', content: 'Три с половиной способа доработки логики Битрикс24. Теория'},
                            {block: 'a', content: 'Практика доработки Битрикс24 тремя способами. Боевой пример'}
                        ]}
                    ]}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Обычное текстовое содержимое
                    {block: 'h', size: '2', content: 'Заголовок в тексте!!!!!!!!!!!!!!!!'},
                    {block: 'p', content: 'В статье рассказано, как это сделать с помощью Битрикс24. Советуем прочитать Манифест внедрения Битрикс24 , где кратко изложены основные принципы работы – “заповеди”.  Для опытных администраторов и IT-сотрудников крупных компаний рекомендуем изучить очень качественный (хотя объемный и сложный) учебный курс по внедрению портала от 1С-Битрикс. '},
                    {block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}
                ]
            }),
            
            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content-long.bemjson.js')({
                bgColor: 'gray-lighter', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры используйте false если заголовок единственный элемент блока
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        sub: 'То о чем мы говорим и то что вы еще не успели прочитать',
                        content: [
                            {block: 'h', size: '3', content: [
                                'Интересное по теме'
                            ]},
                        ]
                    }),
                    require('../../../bem/pages/_common/longrids/topic/topic.bemjson.js')
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content-long.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    require('../../../bem/pages/_common/longrids/people/people.bemjson.js')
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: 'gray-lighter', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры используйте false если заголовок единственный элемент блока
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        sub: ' ',
                        content: [
                            {block: 'h', size: '2', content: [
                                'Наши работы',
                                {block: 'icon', icon: 'case'}
                            ]}
                        ]
                    }),
                    require('../../../bem/pages/_common/longrids/work/work.bemjson.js')
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры используйте false если заголовок единственный элемент блока
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        sub: ' ',
                        content: [
                            {block: 'h', size: '2', content: [
                                'Отзывы',
                                {block: 'icon', icon: 'review-dot'}
                            ]}
                        ]
                    }),
                    require('../../../bem/pages/_common/longrids/review/review.bemjson.js')
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    {elem: 'flex', mods: {'justify-content': 'space-between'}, content: [
                        require('../../../bem/pages/_common/longrids/tile/tile.bemjson.js')
                    ]}
                ]
            }),

            // Стандартный блок лонгрида почти всегда используется как основа для нового блока исключение блок brick-image
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                src: {
                    xs: '../../../upload/brick-image-xs.jpg',
                    sm: '../../../upload/brick-image-sm.jpg',
                    md: '../../../upload/brick-image-md.jpg',
                    lg: '../../../upload/brick-image-lg.jpg'
                },
                bgColor: 'gray-dark', // имя переменной цаета или оставте пустым
                textColor: 'invert', // допустимые значения [base, invert];
                content: [
                    // Вставка кастомизированого заголовка для brick-content
                    // sub, не обязательные параметры используйте false если заголовок единственный элемент блока
                    require('../../../bem/pages/_common/longrids/brick-content/func-brick-content__header.bemjson.js')({
                        sub: ' ',
                        content: [
                            {block: 'h', size: '2', content: [
                                'Оставить отзыв',
                                {block: 'icon', icon: 'review-o'}
                            ]}
                        ]
                    }),
                    require('../../../bem/pages/_common/forms/review.bemjson.js')
                ]
            })

        ]},
        require('../../../bem/pages/_common/footer.bemjson.js'),
        require('../../../bem/pages/_common/modals/mail.bemjson.js')
    ]
};
