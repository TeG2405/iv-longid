module.exports = {
    block: 'page',
    title: 'Блог на теме лонгрида',
    id: 'blog',
    styles: [{elem: 'css', url: '../_merged/_merged.css'}],
    scripts: [{elem: 'js', url: '../_merged/_merged.async.js', async: true},
        {elem: 'js', url: '../_merged/_merged.js'}, {elem: 'js', url: '../_merged/_merged.i18n.ru.js'}],
    content: [
        require('../../../bem/pages/_common/header.bemjson.js'),
        {block: 'main', content: [
            require('../../../bem/pages/_common/longrids/brick-content/func-brick-content.bemjson.js')({
                bgColor: '', // имя переменной цаета или оставте пустым
                textColor: 'base', // допустимые значения [base, invert];
                content: [
                    {block: 'read-progress',  content: [
                        {elem: 'bar'},
                        {elem: 'sidebar', content: [
                            {elem: 'sidebar-inner', content: [
                                {elem: 'control', content: {
                                    block: 'fa', icon: 'list'
                                }},
                                {elem: 'list', mix: {elem: 'map'}, content: [
                                    {elem: 'li', content: [
                                        {elem: 'link', attrs: {href: '#nav-section-1'}, content: 'Заглолвок 1'}
                                    ]},
                                    {elem: 'li', content: [
                                        {elem: 'link', attrs: {href: '#nav-section-2'}, content: 'Заглолвок 2'},
                                        {elem: 'list', content: [
                                            {elem: 'li', content: [
                                                {elem: 'link', attrs: {href: '#nav-section-2-1'}, content: 'Заглолвок 2.1'}
                                            ]},
                                            {elem: 'li', content: [
                                                {elem: 'link', attrs: {href: '#nav-section-2-2'}, content: 'Заглолвок 2.2'}
                                            ]},
                                            {elem: 'li', content: [
                                                {elem: 'link', attrs: {href: '#nav-section-2-3'}, content: 'Заглолвок 2.3'}
                                            ]},
                                            {elem: 'li', content: [
                                                {elem: 'link', attrs: {href: '#nav-section-2-4'}, content: 'Заглолвок 2.4'}
                                            ]}
                                        ]}
                                    ]},
                                    {elem: 'li', content: [
                                        {elem: 'link', attrs: {href: '#nav-section-3'}, content: 'Заглолвок 3'}
                                    ]},
                                    {elem: 'li', content: [
                                        {elem: 'link', attrs: {href: '#nav-section-4'}, content: 'Заглолвок 4'}
                                    ]}
                                ]}
                            ]}
                        ]},
                        {elem: 'container', content: [
                            {block: 'h', size: '2', attrs:{id: 'nav-section-1'}, content: [ 'Заглолвок 1' ]},
                            (function (i) {
                                return [i, i,  i, i, i, i, i, i, i, i]
                            })({block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}),
                            {block: 'h', size: '2', attrs:{id: 'nav-section-2'}, content: [ 'Заглолвок 2' ]},
                            {block: 'h', size: '3', attrs:{id: 'nav-section-2-1'}, content: [ 'Заглолвок 2.1' ]},
                            (function (i) {
                                return [i, i,  i, i, i, i, i, i, i, i]
                            })({block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}),
                            {block: 'h', size: '3', attrs:{id: 'nav-section-2-2'}, content: [ 'Заглолвок 2.2' ]},
                            (function (i) {
                                return [i, i,  i, i, i, i, i, i, i, i]
                            })({block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}),
                            {block: 'h', size: '3', attrs:{id: 'nav-section-2-3'}, content: [ 'Заглолвок 2.3' ]},
                            (function (i) {
                                return [i, i,  i, i, i, i, i, i, i, i]
                            })({block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}),
                            {block: 'h', size: '3', attrs:{id: 'nav-section-2-4'}, content: [ 'Заглолвок 2.4' ]},
                            (function (i) {
                                return [i, i,  i, i, i, i, i, i, i, i]
                            })({block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}),
                            (function (i) {
                                return [i, i,  i, i, i, i, i, i, i, i]
                            })({block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}),
                            {block: 'h', size: '2', attrs:{id: 'nav-section-3'}, content: [ 'Заглолвок 3' ]},
                            (function (i) {
                                return [i, i,  i, i, i, i, i, i, i, i]
                            })({block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}),
                            {block: 'h', size: '2', attrs:{id: 'nav-section-4'}, content: [ 'Заглолвок 4' ]},
                            (function (i) {
                                return [i, i,  i, i, i, i, i, i, i, i]
                            })({block: 'p', content: 'Эта статья описывает последовательность шагов внедрения Битрикс24 за 7 шагов-“дней”. Каждый шаг — не 24 часа, а более протяженный период времени, обычно 1-2 рабочие недели. Будем говорить о маленькой или средней компании без опыта серьезной автоматизации процессов.'}),
                        ]}
                    ]}
                ]
            }),
            require('../../../bem/pages/_common/longrids/brick-image/func-brick-image.bemjson.js')({
                header: 'Разработка сайтов',
                description: [{block: 'i', content: [
                    'Мы занимаемся запуском веб-проектов. Обычно это довольно крупные корпоративные сайты или интернет-магазины. Иногда — социальные сети, аукционы или биржи товаров и услуг.'
                ]}],
                src: {
                    xs: '../../../upload/brick-image-xs.jpg',
                    sm: '../../../upload/brick-image-sm.jpg',
                    md: '../../../upload/brick-image-md.jpg',
                    lg: '../../../upload/brick-image-lg.jpg'
                },
                offset: 2,
                icon: true
            }),
            require('../../../bem/pages/_common/longrids/brick-image/func-brick-image.bemjson.js')({
                header: 'Разработка сайтов',
                description: [{block: 'i', content: [
                    'Мы занимаемся запуском веб-проектов. Обычно это довольно крупные корпоративные сайты или интернет-магазины. Иногда — социальные сети, аукционы или биржи товаров и услуг.'
                ]}],
                src: {
                    xs: '../../../upload/brick-image-xs.jpg',
                    sm: '../../../upload/brick-image-sm.jpg',
                    md: '../../../upload/brick-image-md.jpg',
                    lg: '../../../upload/brick-image-lg.jpg'
                },
                offset: 2,
                icon: true
            }),
            require('../../../bem/pages/_common/longrids/brick-image/func-brick-image.bemjson.js')({
                header: 'Разработка сайтов',
                description: [{block: 'i', content: [
                    'Мы занимаемся запуском веб-проектов. Обычно это довольно крупные корпоративные сайты или интернет-магазины. Иногда — социальные сети, аукционы или биржи товаров и услуг.'
                ]}],
                src: {
                    xs: '../../../upload/brick-image-xs.jpg',
                    sm: '../../../upload/brick-image-sm.jpg',
                    md: '../../../upload/brick-image-md.jpg',
                    lg: '../../../upload/brick-image-lg.jpg'
                },
                offset: 2,
                icon: true
            }),
        ]},
        require('../../../bem/pages/_common/footer.bemjson.js'),
        require('../../../bem/pages/_common/modals/mail.bemjson.js')
    ]
};
