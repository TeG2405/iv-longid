module.exports = function (bh) {
    bh.match('footer__mail', function (ctx, json) {
        ctx.tag('a').attrs({
            href: 'mailto:'+ctx.content()
        });
    })
}