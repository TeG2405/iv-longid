module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {
        bh._options.replacements['footer__container'] = 'container';
        bh._options.replacements['footer__row'] = 'row';
        ctx.tag('footer');
    });
};