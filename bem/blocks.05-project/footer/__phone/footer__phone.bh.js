module.exports = function (bh) {
    bh.match('footer__phone', function (ctx, json) {
        ctx.tag('a').attrs({
            href: 'tel:'+json.mask
        });
        ctx.content("Тел. "+ ctx.content(),true);
    })
}