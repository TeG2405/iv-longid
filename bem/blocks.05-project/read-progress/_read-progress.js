/**
 * Created by Intervolga.ru on 20.06.2017.
 */
+function($){
    var Block = function(element, option){
        this.$block = null;
        this.$bar = null;
        this.$map = null;
        this.$sidebar = null;
        this.$sidebarInner = null;
        this.$points = null;
        this.$navs = null;
        this.scrollPosition = null;
        this.blockPosition = null;
        this.blockHeight = null;
        this.blockWidth = null;
        this.sidebarHeight = null;
        this.sidebarWidth = null;
        this.sidebarInnerHeight = null;
        this.sidebarInnerWidth = null;
        this.ticking = false;
        this.init(element, option);
    };

    Block.prototype.init = function(element, option){
        this.body = $('body,html');
        this.$block = $(element);
        this.$bar = this.$block.find('.read-progress__bar');
        this.$map = this.$block.find('.read-progress__map');
        this.$sidebar = this.$block.find('.read-progress__sidebar');
        this.$sidebarInner = this.$block.find('.read-progress__sidebar-inner');
        this.$navs = this.$map.find('.read-progress__link');
        this.$points = this.$block.find('[id^=nav]');

        this.$navs.on('click', $.proxy(function (e) {
            this.ticking = true;
            this.body.animate({ scrollTop: this.$points.filter(e.target.hash).offset().top }, 500, $.proxy(function () {
                this.ticking = false;
                window.requestAnimationFrame($.proxy(function() {
                    this.onScroll(e);
                    this.ticking = false;
                }, this));
            }, this));
            e.preventDefault();
        }, this));
        $(window).on('resize scroll load', $.proxy(function (e) {
            var EventType = e.type;
            if(EventType == 'scroll') this.scrollPosition = window.scrollY;
            if(EventType == 'resize' || EventType == 'load'){
                this.blockPosition = this.$block.offset().top;
                this.blockHeight = this.$block.outerHeight();
                this.blockWidth = this.$block.outerWidth();
                this.sidebarHeight = this.$sidebar.outerHeight();
                this.sidebarWidth = this.$sidebar.outerWidth();
                this.sidebarInnerHeight = this.$sidebarInner.outerHeight();
                this.sidebarInnerWidth = this.$sidebarInner.outerWidth();
            }
            if(!this.ticking){
                window.requestAnimationFrame($.proxy(function() {
                    this.onScroll(e);
                    this.ticking = false;
                }, this));
            }
            this.ticking = true;
        }, this));

    };
    Block.prototype.onScroll = function (e) {
        var EventType = e.type;
        // Инициализация;
        if(EventType == 'load') this.$block.addClass('read-progress_init');

        // Рачет класа fixed;
        this.scrollPosition < this.blockPosition + this.$sidebar.position().top ? this.$block.removeClass('read-progress_fixed') : this.$block.addClass('read-progress_fixed');

        // Рачет класа stop;
        if(this.scrollPosition < this.$sidebar.offset().top){
            this.$block.removeClass('read-progress_stop');
        }else{
            if(this.scrollPosition >= this.blockPosition + this.blockHeight - (this.sidebarInnerHeight + this.$sidebarInner.position().top)){
                this.$block.addClass('read-progress_stop');
            }
        }

        // Пересчет максимальной ширины либо класс для мобильной версии;
        var width = this.body.outerWidth() < window.innerWidth ? this.body.outerWidth() : window.innerWidth;
        width = (width - this.blockWidth) / 2;
        if(this.sidebarWidth < this.sidebarInnerWidth) this.$sidebar.addClass('mobile').css({ 'width': 'auto' });
        else this.$sidebar.removeClass('mobile').css({ 'width': width+'px' });


        //Пересчет прогресс бара
        var progress = (this.scrollPosition - this.blockPosition + window.innerHeight) * 100 / this.blockHeight;
        if(progress < 0) progress = 0;
        if(progress > 100) progress = 100;
        this.$bar.css({ 'width': progress + '%' });

        // Пролистан до конца
        this.scrollPosition > this.blockPosition + this.blockHeight ? this.$block.addClass('read-progress_outer') : this.$block.removeClass('read-progress_outer');

        // Выбор активного пункта навигации
        this.$points.each($.proxy(function(i){
            var $this = this.$points.eq(i);
            if($this.offset().top <= this.scrollPosition + 10) {
                this.$navs
                    .parent()
                    .removeClass('read-progress__li_active')
                    .eq(i)
                    .addClass('read-progress__li_active')
                    .parent()
                    .parent('.read-progress__li')
                    .addClass('read-progress__li_active');
            }
        }, this));
    };

    function Plugin(option){
        return this.each(function () {
            var $this   = $(this);
            var data    = $this.data('block.read_progress');
            var options = typeof option == 'object' && option;

            if (!data) $this.data('block.read_progress', (data = new Block(this, options)));
            if (typeof option == 'string') data[option]();
        })
    }

    $.fn.block_read_progress = Plugin;
    $.fn.block_read_progress.Constructor = Block;
}(jQuery);

$('.read-progress').block_read_progress();
