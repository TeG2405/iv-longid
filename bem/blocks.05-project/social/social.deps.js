({
    mustDeps: [
        {
            block: 'fa'
        }
    ],
    shouldDeps: [
        {
            block: 'social',
            elem: ['link', 'item'],
            mods: ['bg', 'bitrix']
        }
    ]
})