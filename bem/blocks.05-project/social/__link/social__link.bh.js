module.exports = function (bh) {
    bh.match('social__link', function (ctx, json) {
        ctx.tag('a');
        ctx.attrs({
            href: ctx.content()
        });
        ctx.content([
            {
                block: "fa",
                mix: {
                    block: ctx.tParam("fa")
                }
            }
        ], true);
    })
}