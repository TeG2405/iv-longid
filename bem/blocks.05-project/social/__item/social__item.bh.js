module.exports = function (bh) {
    bh.match('social__item', function (ctx, json) {
        ctx.tParam('fa', 'fa-'+json.fa);
        ctx.tag('li');
        if(ctx.mods().bitrix){
            ctx.content('Быстро с битрикс', true);
        }else{
            ctx.content([
                {
                    elem: 'link',
                    mods:{
                        bg: json.fa
                    },
                    content: ctx.content()
                }
            ], true);

        }

    })
}