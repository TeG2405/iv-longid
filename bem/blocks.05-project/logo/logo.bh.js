module.exports = function (bh) {
    bh.match('logo', function (ctx, json) {
        ctx.tag('a').attr('href', '#');
        ctx.content([
            {
                elem: "name",
                content: [
                    {
                        block: 'picture',
                        src: json.src
                    }
                ]
            },
            {
                elem: 'slogan',
                content: ctx.content()
            }
        ], true);
    })
}