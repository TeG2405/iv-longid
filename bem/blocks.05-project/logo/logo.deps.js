({
    mustDeps: [
        { block: 'picture' }
    ],
    shouldDeps: [

        {
            block: 'logo',
            elem: ['name', 'slogan']
        }
    ]
})