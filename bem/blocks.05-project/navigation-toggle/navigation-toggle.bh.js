module.exports = function (bh) {
    bh.match('navigation-toggle', function (ctx, json) {
        var BlockID = 'navigation-'+ctx.generateId();
        if(json.navigationID !== undefined){
            BlockID = json.navigationID;
        }
        ctx.attrs({
            "data-toggle":"collapse",
            "data-target": "#"+BlockID,
            "aria-expanded": false,
            "type": "button"
        });
        ctx.tag('button').cls('collapsed');
        ctx.content([
            {elem: "icon"},
            {elem: "icon"},
            {elem: "icon"}
        ]);
    })
}