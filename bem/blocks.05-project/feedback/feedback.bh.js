module.exports = function (bh) {
    bh.match('feedback', function (ctx, json) {
        var BlockID = 'feedback-'+ctx.generateId();
        if(json.modalID !== undefined){
            BlockID = json.modalID;
        }

        ctx.tag('button').attrs({
            "type": "button",
            "data-toggle": "modal",
            "data-target":"#"+BlockID
        });
        ctx.content([
            {
                block: "fa",
                tag: "i",
                mix: [{block: "fa-envelope-o"}]
            },
            {
                elem: "label",
                tag: "span",
                content: ctx.content()
            }
        ], true);

    })
}