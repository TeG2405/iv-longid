module.exports = function (bh) {
    bh.match('navigation', function (ctx, json) {
        var BlockID = 'navigation-'+ctx.generateId();
        if(json.navigationID !== undefined){
            BlockID = json.navigationID;
        }

        ctx.tParam('BlockID', BlockID);
    })
}