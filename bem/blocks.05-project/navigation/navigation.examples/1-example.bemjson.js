({
    block: "navigation",
    content: [
        {
            elem: "container",
            content: [
                {
                    elem: "header",
                    content: "Шапка"
                },
                {
                    elem: "nav",
                    content: [
                        {
                            elem: "item",
                            mods: {'state': 'active'},
                            content: "Портфолио"
                        },
                        {
                            elem: "item",
                            content: "Отзывы"
                        },
                        {
                            elem: "item",
                            content: "1С-Битрикс"
                        },
                        {
                            elem: "item",
                            content: "Блог"
                        },
                        {
                            elem: "item",
                            content: "Цены"
                        },
                        {
                            elem: "item",
                            content: "Мы"
                        },
                        {
                            elem: "item",
                            content: "Тиражные решения"
                        },
                        {
                            elem: "item",
                            content: "Вакансии"
                        },
                        {
                            elem: "item",
                            content: "Контакты"
                        }
                    ]
                }
            ]
        }
    ]
})
