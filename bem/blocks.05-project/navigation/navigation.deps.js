({
    mustDeps: [
        {
            block: ''
        }
    ],
    shouldDeps: [
        {
            block: 'navigation',
            elem: ['link', 'toggle', 'icon', 'collapse', 'helper']
        }
    ]
})