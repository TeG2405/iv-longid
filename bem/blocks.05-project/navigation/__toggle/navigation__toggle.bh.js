module.exports = function (bh) {
    bh.match('navigation__toggle', function (ctx, json) {
        ctx.attrs({
            "data-toggle":"collapse",
            "data-target": "#"+ctx.tParam('BlockID'),
            "aria-expanded": false,
            "type": "button"
        }).tag('button');
    })
}