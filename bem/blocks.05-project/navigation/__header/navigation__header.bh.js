module.exports = function (bh) {
    bh.match('navigation__header', function (ctx, json) {
        var tpl = [
            {
                elem: "toggle",
                content: [
                    {elem: "icon"},
                    {elem: "icon"},
                    {elem: "icon"}
                ]
            }
        ];
        ctx.content(tpl, true);
    })
}