+function ($) {
    'use strict';
    var $navigation = $('.navigation');
    var $collapse = $navigation.find('.collapse');

    var clear = function(data){
        var $target = $(data.target);
        if($target.hasClass('navigation__shadow')){
            $collapse.collapse('hide');
        }
    };
    $(document).on('click.navigation', clear);
}(jQuery);
