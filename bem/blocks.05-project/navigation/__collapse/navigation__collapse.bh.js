module.exports = function (bh) {
    bh.match('navigation__collapse', function (ctx, json) {
        ctx.tag('nav').cls('collapse').attrs({
            id: ctx.tParam('BlockID')
        });
    })
}