module.exports = function (bh) {
    bh.match('phone-list', function (ctx, json) {
        var _list = [];
        for(var i=0; i<json.numbers.length; i++){
            _list[i] =  {
                elem: "item",
                content: [
                    {
                        elem: "prefix",
                        tag: "span",
                        content: json.numbers[i].prefix
                    },
                    {
                        elem: "number",
                        tag: "span",
                        content: json.numbers[i].number
                    }
                ]
            }
        }
        ctx.content(_list, true);
    })
}