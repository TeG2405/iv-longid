({
    mustDeps: {block: ''},
    shouldDeps: [
        {block: 'phone-list'},
        {
            block: 'phone-list',
            elem: ['number', 'prefix', 'item']
        },
    ]
})