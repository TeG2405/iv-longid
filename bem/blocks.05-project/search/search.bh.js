module.exports = function (bh) {
    bh.match('search', function (ctx, json) {
        ctx.tag('form');
        ctx.content([
            {
                elem: "input",
                tag: "input",
                attrs: {
                    type: "text",
                    placeholder: "Поиск"
                }
            },
            {
                elem: "button",
                tag: "button",
                attrs: {
                    type: "button"
                },
                content: {
                    block: "fa",
                    mix: {block: "fa-search"},
                    tag: "i"
                }
            }
        ], true);
    })
}