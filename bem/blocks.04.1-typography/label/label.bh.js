module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {

        var isBEM = Object.keys(ctx.mods()).length > 0;

        ctx.bem(isBEM).tag('label');
    });
};