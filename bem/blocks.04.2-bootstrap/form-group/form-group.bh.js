module.exports = function (bh) {
    bh.match(module.id.replace('.bh.js', '').match(/([^\\]+)?$/), function (ctx, json) {
        var content = [];
        if(json.arParam){
            json.arParam.label && content.push([
                {block: 'control-label', mix: [
                    {block: 'col-xs-' + (12 - json.arParam.grid[0] || 12)},
                    {block: 'col-sm-' + (12 - json.arParam.grid[1] || 12)},
                    {block: 'col-md-' + (12 - json.arParam.grid[2] || 12)},
                    {block: 'col-lg-' + (12 - json.arParam.grid[3] || 12)}
                ], content: [
                    json.arParam.label
                ]}
            ]);

            ctx.content() && content.push([
                {mix: [
                    {block: 'col-xs-' + (json.arParam.grid[0] || 12)},
                    {block: 'col-sm-' + (json.arParam.grid[1] || 12)},
                    {block: 'col-md-' + (json.arParam.grid[2] || 12)},
                    {block: 'col-lg-' + (json.arParam.grid[3] || 12)},

                    json.arParam.label || {block: 'col-xs-offset-' + (12 - json.arParam.grid[0] || 0)},
                    json.arParam.label || {block: 'col-sm-offset-' + (12 - json.arParam.grid[1] || 0)},
                    json.arParam.label || {block: 'col-md-offset-' + (12 - json.arParam.grid[2] || 0)},
                    json.arParam.label || {block: 'col-lg-offset-' + (12 - json.arParam.grid[3] || 0)}
                ], content: [
                    ctx.content()
                ]}
            ]);
            ctx.content([
                content
            ], !!json.arParam)
        }
    });
};